const express = require("express");
const connection = require("../mysql/mysqlconf");
const mysql = require("mysql");
const router = express.Router();

/**
 * GET mode with query
 *      name - name we wan't to find
 * RETURN status 200
 *      mode object (id, name)
 * RETURN status 204
 *      id: -1
 * URL(GET) : <HOST>/mode?name=<MODE_NAME>
 */
router.get("/mode", (req, res) => {
  const { name } = req.query;
  const GET_COUNT_FOR_MODE_BY_NAME = mysql.format(
    "select * from Mode where name=?",
    [name]
  );
  connection.query(GET_COUNT_FOR_MODE_BY_NAME, (err, result) => {
    if (err) {
      return res.status(500).send(err);
    }
    if (result.length > 0) {
      console.log(result);
      return res.status(200).json({
        ...result[0]
      });
    } else {
      return res.status(204).json({
        id: -1
      });
    }
  });
});

module.exports = router;
