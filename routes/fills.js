const express = require("express");
const connection = require("../mysql/mysqlconf");
const mysql = require("mysql");
const router = express.Router();

connection.connect(err => {
  if (err) {
    return err;
  }
});

/**
 * GET all fills with no query
 * RETURN status 200
 *      devices : array of device fill (id, name, start, end)
 * RETURN status 404
 *      devices : [] - empty array
 * URL(GET) : <HOST>/fills
 */
router.get("/fills", (req, res) => {
  const GET_FILLS_METADATA = mysql.format("select * from Fill");
  connection.query(GET_FILLS_METADATA, (err, results) => {
    if (results.length > 0) {
      for (let i = 0; i < results.length; i++) {
        results[i].start = results[i].start.toUTCString();
        results[i].end = results[i].end.toUTCString();
      }
    } else {
      return res.status(204).json({
        metadata: []
      });
    }
    if (err) {
      return res.status(500).send(err);
    } else {
      return res.status(200).json({
        metadata: results
      });
    }
  });
});

/**
 * GET fill by id with query
 *      id - fill_id of the fill we search
 * RETURN status 200
 *      metadata : fill object (id, name, start, end)
 * RETURN status 404
 *      metadata : {id: -1, ...}
 * URL(GET) : <HOST>/fill/metadata/id?id=<FILL_ID>
 */
router.get("/fill/metadata/id", (req, res) => {
  const { id } = req.query;
  const GET_METADATA_BY_FILL_ID = mysql.format(
    "select * from Fill where id=?",
    [id]
  );
  connection.query(GET_METADATA_BY_FILL_ID, (err, results) => {
    if (results.length > 0) {
      results[0].start = results[0].start.toUTCString();
      results[0].end = results[0].end.toUTCString();
    } else {
      return res.status(204).json({
        metadata: {
          id: -1,
          name: "",
          start: "",
          end: ""
        }
      });
    }

    if (err) {
      return res.status(500).send(err);
    } else {
      return res.status(200).json({
        metadata: results[0]
      });
    }
  });
});

/**
 * GET fill by name with query
 *      name - fill_name of the fill we search
 * RETURN status 200
 *      metadata : fill object (id, name, start, end)
 * RETURN status 204
 *      metadat: {id: -1, ... }
 * URL(GET) : <HOST>/fill/metadata/id?id=<FILL_ID>
 */
router.get("/fill/metadata/name", (req, res) => {
  const { name } = req.query;
  const GET_METADATA_BY_FILL_NAME = mysql.format(
    "select * from Fill where name=?",
    [name]
  );
  connection.query(GET_METADATA_BY_FILL_NAME, (err, results) => {
    if (results.length > 0) {
      results[0].start = results[0].start.toUTCString();
      results[0].end = results[0].end.toUTCString();
    } else {
      return res.status(204).json({
        metadata: {
          id: -1,
          name: "",
          start: "",
          end: ""
        }
      });
    }
    if (err) {
      return res.status(500).send(err);
    } else {
      return res.status(200).json({
        metadata: results[0]
      });
    }
  });
});

/**
 * GET modes of a fill by id with query
 *      id - fill_id of the fill we search
 * RETURN status 200
 *      modes : array of modes object (id, name, start, end)
 * RETURN status 204
 *      modes : [] empty array
 * URL(GET) : <HOST>/fill/modes/id?id=<FILL_ID>
 */
router.get("/fill/modes/id", (req, res) => {
  const { id } = req.query;
  const GET_MODES_BY_FILL_ID = mysql.format(
    "select M.name, mode_id, FillMode.id, start, end from FillMode inner join Mode M on FillMode.mode_id = M.id where fill_id=? order by start;",
    [id]
  );
  connection.query(GET_MODES_BY_FILL_ID, (err, results) => {
    if (results.length > 0) {
      for (let i = 0; i < results.length; i++) {
        results[i].start = results[i].start.toUTCString();
        results[i].end = results[i].end.toUTCString();
      }
    } else {
      return res.status(204).json({
        modes: []
      });
    }

    if (err) {
      return res.status(500).send(err);
    } else {
      return res.status(200).json({
        modes: results
      });
    }
  });
});

/**
 * GET modes of a fill by name with query
 *      name - fill_name of the fill we search
 * RETURN status 200
 *      modes : array of modes object (id, name, start, end)
 * RETURN status 204
 *      modes : [] empty array
 * URL(GET) : <HOST>/fill/modes/name?name=<FILL_NAME>
 */
router.get("/fill/modes/name", (req, res) => {
  const { name } = req.query;
  const GET_MODES_BY_FILL_NAME = mysql.format(
    "select M.name, mode_id, FillMode.id, FillMode.start, FillMode.end from FillMode inner join Mode M on FillMode.mode_id = M.id inner join Fill F on FillMode.fill_id = F.id where F.name=? order by FillMode.start;",
    [name]
  );
  connection.query(GET_MODES_BY_FILL_NAME, (err, results) => {
    if (results.length > 0) {
      for (let i = 0; i < results.length; i++) {
        results[i].start = results[i].start.toUTCString();
        results[i].end = results[i].end.toUTCString();
      }
    } else {
      return res.status(204).json({
        modes: []
      });
    }
    if (err) {
      return res.status(500).send(err);
    } else {
      return res.status(200).json({
        modes: results
      });
    }
  });
});

/**
 * GET devices of a fill by id with query
 *      id - fill_id of the fill we search
 * RETURN status 200
 *      devices : array of devices object (id, name, start, end)
 * RETURN status 404
 *      devices : [] empty array
 * URL(GET) : <HOST>/fill/devices/id?id=<FILL_ID>
 */
router.get("/fill/devices/id", (req, res) => {
  const { id } = req.query;
  const GET_DEVICES_BY_FILL_ID = mysql.format(
    "select  distinct D.name, D.id  from Measure " +
      "inner join Device D on Measure.device_id = D.id " +
      "inner join FillMode FM on Measure.fill_mode_id = FM.id " +
      "inner join Fill F on FM.fill_id=F.id where F.id =? ",
    [id]
  );
  connection.query(GET_DEVICES_BY_FILL_ID, (err, results) => {
    if (err) {
      return res.status(500).send(err);
    } else {
      return res.json({
        devices: results
      });
    }
  });
});

/**
 * GET devices of a fill by id with query
 *      name - fill_id of the fill we search
 * RETURN status 200
 *      devices : array of device object (id, name)
 * RETURN status 404
 *      devices : [] empty array
 * URL(GET) : <HOST>/fill/devices/name?name=<FILL_NAME>
 */
router.get("/fill/devices/name", (req, res) => {
  const { name } = req.query;
  const GET_DEVICES_BY_FILL_NAME = mysql.format(
    "select  distinct D.name, D.id  from Measure " +
      "inner join Device D on Measure.device_id = D.id " +
      "inner join FillMode FM on Measure.fill_mode_id = FM.id " +
      "inner join Fill F on FM.fill_id=F.id where F.name =? ",
    [name]
  );
  connection.query(GET_DEVICES_BY_FILL_NAME, (err, results) => {
    if (err) {
      return res.status(500).send(err);
    } else {
      return res.status(200).json({
        devices: results
      });
    }
  });
});

/**
 * GET last fill with no query
 * RETURN status 200
 *      fill object (id, fill-name, start, end)
 * RETURN status 204
 *      id: -1
 * URL(GET) : <HOST>/fill/last
 */
router.get("/fill/last", (req, res) => {
  const GET_LAST_FILL = mysql.format(
    "select * from Fill order by name DESC limit 1"
  );
  connection.query(GET_LAST_FILL, (err, result) => {
    if (err) {
      return res.status(500).send(err);
    }
    if (result.length > 0)
      return res.status(200).json({
        ...result[0]
      });
    else
      return res.status(204).json({
        id: -1
      });
  });
});

/**
 * CREATE new fill with query
 *      name - name of the fill we wan't to create
 *      start - string date /!\ no spaces allowed in URL --> use '_' instead
 *      end - string date /!\ no spaces allowed in URL --> use '_' instead
 * RETURN status 201
 *      id : id of newly created fill
 * URL(POST) : <HOST>/new_fill?name=<FILL_NAME>&start=<START>&end=<END>
 */
router.post("/new_fill", (req, res) => {
  const { name, start, end } = req.query;
  const CREATE_NEW_FILL = mysql.format(
    "insert into Fill(name, start, end) values (?, ?, ?)",
    [name, start, end]
  );
  if (name && start && end)
    connection.query(CREATE_NEW_FILL, (err, result) => {
      if (err) return res.status(500).send(err);
      else {
        return res.status(201).json({
          id: result.insertId
        });
      }
    });
  else return res.sendStatus(400);
});

module.exports = router;
