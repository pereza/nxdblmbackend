const express = require("express");
const connection = require("../mysql/mysqlconf");
const mysql = require("mysql");
const router = express.Router();

/**
 * GET device with query
 *      name - name of the device we wan't to find
 * RETURN status 200
 *      device object (id, name)
 * RETURN status 204
 *      id: -1
 * URL(GET) : <HOST>/device?name=<DEVICE_NAME>
 */
router.get("/device", (req, res) => {
  const { name } = req.query;
  const GET_DEVICE_ID_BY_NAME = mysql.format(
    "SELECT * FROM Device where name=? limit 1",
    [name]
  );
  connection.query(GET_DEVICE_ID_BY_NAME, (err, result) => {
    if (err) {
      return res.status(500).send(err);
    }
    if (result.length > 0) {
      return res.status(200).json({
        ...result[0]
      });
    } else {
      return res.status(204).json({
        id: -1
      });
    }
  });
});

/**
 * GET all devices with no query
 * RETURN status 200
 *      devices : array of device objects (id, name)
 * RETURN status 204
 *      devices : [] - empty array
 * URL(GET) : <HOST>/devices
 */
router.get("/devices", (req, res) => {
  const GET_DEVICES = mysql.format("select * from Device");
  connection.query(GET_DEVICES, (err, result) => {
    if (err) {
      return res.status(500).send(err);
    }
    if (result.length > 0) {
      return res.status(200).json({
        devices: result
      });
    } else {
      return res.status(204).json({
        devices: []
      });
    }
  });
});

module.exports = router;
