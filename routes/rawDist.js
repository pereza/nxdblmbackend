const express = require("express");
const connection = require("../mysql/mysqlconf");
const mysql = require("mysql");
const router = express.Router();

/**
 * GET raw_dist_hg datapoints and number of rows of a fill by id with query
 *      fill_id - fill_id of the fill we search
 *      fill_mode_id - id of the fill_mode we wan't to look at
 *      devive_id - id of the device we can't data
 * RETURN status 200
 *      rows : number of rows in the raw_dist
 *      datapoints : array of data for raw_dist
 * RETURN status 204
 *      rows : 0
 *      datapoints : [] empty array
 * URL(GET) : <HOST>/raw_dist/hg?fill_id=<FILL_ID>&fill_mode_id=<FILL_MODE_ID>&device_id=<DEVICE_ID>
 */
router.get("/raw_dist/hg", (req, res) => {
  const { fill_id, fill_mode_id, device_id } = req.query;
  const GET_RAW_DIST_HG = mysql.format(
    "select raw_dist from Data where id=( " +
      "select data_id from Measure " +
      "inner join FillMode FM on Measure.fill_mode_id = FM.id " +
      "where device_id=? and fill_id=? and fill_mode_id=? " +
      ")",
    [device_id, fill_id, fill_mode_id]
  );
  connection.query(GET_RAW_DIST_HG, (err, results) => {
    if (results.length > 0) {
      results = JSON.parse(results[0].raw_dist);
    } else {
      return res.status(204).json({
        rows: 0,
        dataPoints: []
      });
    }
    if (err) {
      return res.status(500).send(err);
    } else {
      return res.status(200).json({
        rows: results.rows,
        dataPoints: results.distribution_hg
      });
    }
  });
});

/**
 * GET raw_dist_lg datapoints and number of rows of a fill by id with query
 *      fill_id - fill_id of the fill we search
 *      fill_mode_id - id of the fill_mode we wan't to look at
 *      devive_id - id of the device we can't data
 * RETURN status 200
 *      rows : number of rows in the raw_dist
 *      datapoints : array of data for raw_dist
 * RETURN status 204
 *      rows : 0
 *      datapoints : [] empty array
 * URL(GET) : <HOST>/raw_dist/lg?fill_id=<FILL_ID>&fill_mode_id=<FILL_MODE_ID>&device_id=<DEVICE_ID>
 */
router.get("/raw_dist/lg", (req, res) => {
  const { fill_id, fill_mode_id, device_id } = req.query;
  const GET_RAW_DIST_LG = mysql.format(
    "select raw_dist from Data where id=( " +
      "select data_id from Measure " +
      "inner join FillMode FM on Measure.fill_mode_id = FM.id " +
      "where device_id=? and fill_id=? and fill_mode_id=? " +
      ")",
    [device_id, fill_id, fill_mode_id]
  );
  connection.query(GET_RAW_DIST_LG, (err, results) => {
    if (err) {
      return res.status(500).send(err);
    } else {
      if (results.length > 0) {
        results = JSON.parse(results[0].raw_dist);
        return res.status(200).json({
          rows: results.rows,
          dataPoints: results.distribution_lg
        });
      } else {
        return res.status(204).json({
          rows: 0,
          dataPoints: []
        });
      }
    }
  });
});

/**
 * UPDATE raw_dist with query
 *      id - id of the data row we wan't to update
 *              and body
 *      data - raw_dist object
 * RETURN status 200 - raw_dist data updated
 * RETURN other status - Error during update
 * URL(PUT) : <HOST>/raw_dist?id=<DATA_ID>
 */
router.put("/raw_dist", (req, res) => {
  const { id } = req.query;
  const data = req.body;
  const UPDATE_RAW_DIST_FOR_DATA_ID = mysql.format(
    "UPDATE Data SET raw_dist = ? where id = ?",
    [JSON.stringify(data), id]
  );
  connection.query(UPDATE_RAW_DIST_FOR_DATA_ID, (err, result) => {
    if (err) return res.status(500).send(err);
    else return res.sendStatus(200);
  });
});
module.exports = router;
