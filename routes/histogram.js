const express = require("express");
const connection = require("../mysql/mysqlconf");
const mysql = require("mysql");
const router = express.Router();

/**
 * GET histogram_hg datapoints and number of rows of a fill by id with query
 *      fill_id - fill_id of the fill we search
 *      fill_mode_id - id of the fill_mode we wan't to look at
 *      devive_id - id of the device we can't data
 * RETURN status 200
 *      rows : number of rows in the histogram
 *      datapoints : array of data for histogram
 * RETURN status 204
 *      rows : 0
 *      datapoints : [] empty array
 * URL(GET) : <HOST>/histogram/hg?fill_id=<FILL_ID>&fill_mode_id=<FILL_MODE_ID>&device_id=<DEVICE_ID>
 */
router.get("/histogram/hg", (req, res) => {
  const { fill_id, fill_mode_id, device_id } = req.query;
  const GET_HISTOGRAM_HG = mysql.format(
    "select histogram from Data where id=( " +
      "select data_id from Measure " +
      "inner join FillMode FM on Measure.fill_mode_id = FM.id " +
      "where device_id=? and fill_id=? and fill_mode_id=? " +
      ")",
    [device_id, fill_id, fill_mode_id]
  );
  connection.query(GET_HISTOGRAM_HG, (err, results) => {
    if (results.length > 0) {
      results = JSON.parse(results[0].histogram);
    } else {
      return res.status(204).json({
        rows: 0,
        dataPoints: []
      });
    }
    if (err) {
      return res.status(500).send(err);
    } else {
      return res.status(200).json({
        rows: results.rows,
        dataPoints: results.histogram_hg
      });
    }
  });
});

/**
 * GET histogram_lg datapoints and number of rows of a fill by id with query
 *      fill_id - fill_id of the fill we search
 *      fill_mode_id - id of the fill_mode we wan't to look at
 *      devive_id - id of the device we can't data
 * RETURN status 200
 *      rows : number of rows in the histogram
 *      datapoints : array of data for histogram
 * RETURN status 204
 *      rows : 0
 *      datapoints : [] empty array
 * URL(GET) : <HOST>/histogram/lg?fill_id=<FILL_ID>&fill_mode_id=<FILL_MODE_ID>&device_id=<DEVICE_ID>
 */
router.get("/histogram/lg", (req, res) => {
  const { fill_id, fill_mode_id, device_id } = req.query;
  const GET_HISTOGRAM_LG = mysql.format(
    "select histogram from Data where id=( " +
      "select data_id from Measure " +
      "inner join FillMode FM on Measure.fill_mode_id = FM.id " +
      "where device_id=? and fill_id=? and fill_mode_id=? " +
      ")",
    [device_id, fill_id, fill_mode_id]
  );
  connection.query(GET_HISTOGRAM_LG, (err, results) => {
    if (results.length > 0) {
      results = JSON.parse(results[0].histogram);
    } else {
      return res.status(204).json({
        rows: 0,
        dataPoints: []
      });
    }
    if (err) {
      return res.status(500).send(err);
    } else {
      return res.status(200).json({
        rows: results.rows,
        dataPoints: results.histogram_lg
      });
    }
  });
});

/**
 * GET histogram_hg intime_datapoints, timestamps and number of rows of a fill by id with query
 *      fill_id - fill_id of the fill we search
 *      fill_mode_id - id of the fill_mode we wan't to look at
 *      devive_id - id of the device we can't data
 * RETURN status 200
 *      rows : number of rows in the histogram
 *      datapoints : array of data for histogram
 *      timstamps : array of timestamp
 * RETURN status 204
 *      rows : 0
 *      datapoints : [] empty array
 *      timestamps: [] empty array
 * URL(GET) : <HOST>/histogram/hg_intime?fill_id=<FILL_ID>&fill_mode_id=<FILL_MODE_ID>&device_id=<DEVICE_ID>
 */
router.get("/histogram/hg_intime", (req, res) => {
  const { fill_id, fill_mode_id, device_id } = req.query;
  const GET_HISTOGRAM_HG_INTIME = mysql.format(
    "select histogram from Data where id=( " +
      "select data_id from Measure " +
      "inner join FillMode FM on Measure.fill_mode_id = FM.id " +
      "where device_id=? and fill_id=? and fill_mode_id=? " +
      ")",
    [device_id, fill_id, fill_mode_id]
  );
  connection.query(GET_HISTOGRAM_HG_INTIME, (err, results) => {
    if (results.length > 0) {
      results = JSON.parse(results[0].histogram);
    } else {
      return res.status(204).json({
        rows: 0,
        dataPoints: [],
        timestamps: []
      });
    }
    if (err) {
      return res.status(500).send(err);
    } else {
      return res.status(200).json({
        rows: results.rows,
        dataPoints: results.histogram_hg_intime,
        timestamps: results.timestamps
      });
    }
  });
});

/**
 * GET histogram_lg intime_datapoints, timestamps and number of rows of a fill by id with query
 *      fill_id - fill_id of the fill we search
 *      fill_mode_id - id of the fill_mode we wan't to look at
 *      devive_id - id of the device we can't data
 * RETURN status 200
 *      rows : number of rows in the histogram
 *      datapoints : array of data for histogram
 *      timestamps : array of timestamp
 * RETURN status 204
 *      rows : 0
 *      datapoints : [] empty array
 *      timestamps: [] empty array
 * URL(GET) : <HOST>/histogram/hl_intime?fill_id=<FILL_ID>&fill_mode_id=<FILL_MODE_ID>&device_id=<DEVICE_ID>
 */
router.get("/histogram/lg_intime", (req, res) => {
  const { fill_id, fill_mode_id, device_id } = req.query;
  const GET_HISTOGRAM_LG_INTIME = mysql.format(
    "select histogram from Data where id=( " +
      "select data_id from Measure " +
      "inner join FillMode FM on Measure.fill_mode_id = FM.id " +
      "where device_id=? and fill_id=? and fill_mode_id=? " +
      ")",
    [device_id, fill_id, fill_mode_id]
  );
  connection.query(GET_HISTOGRAM_LG_INTIME, (err, results) => {
    if (results.length > 0) {
      results = JSON.parse(results[0].histogram);
    } else {
      return res.status(204).json({
        rows: 0,
        dataPoints: [],
        timestamps: []
      });
    }
    if (err) {
      return res.status(500).send(err);
    } else {
      return res.status(200).json({
        rows: results.rows,
        dataPoints: results.histogram_lg_intime,
        timestamps: results.timestamps
      });
    }
  });
});

/**
 * UPDATE histogram
 * with query
 *      id - id of the data row we wan't to update
 * and body
 *      data - histogram object
 * RETURN status 200 - histogram data updated
 * RETURN other status - Error during update
 * URL(PUT) : <HOST>/histogram?id=<DATA_ID>
 */
router.put("/histogram", (req, res) => {
  const { id } = req.query;
  const data = req.body;
  const UPDATE_HISTOGRAM_FOR_DATA_ID = mysql.format(
    "UPDATE Data SET histogram = ? where id = ?",
    [JSON.stringify(data), id]
  );
  connection.query(UPDATE_HISTOGRAM_FOR_DATA_ID, (err, result) => {
    if (err) return res.status(500).send(err);
    else return res.sendStatus(200);
  });
});

module.exports = router;
