const express = require("express");
const connection = require("../mysql/mysqlconf");
const mysql = require("mysql");
const router = express.Router();

/**
 * GET data with query
 *      id - id of data we search
 * RETURN status 200
 *      data object (id, fill-name, mode-name, start, end)
 * RETURN status 204
 *      id: -1
 * URL(GET) : <HOST>/data?id=<DATA_ID>
 */
router.get("/data", (req, res) => {
  const { id } = req.query;
  const GET_DATA_BY_ID = mysql.format("select * from Data where id=?", [id]);
  connection.query(GET_DATA_BY_ID, (err, result) => {
    if (err) return res.status(500).send(err);
    if (result.length > 0) {
      return res.status(200).json({
        ...result
      });
    } else
      return res.status(204).json({
        id: -1
      });
  });
});

/**
 * GET completion with query
 *      id - id of data we search
 * RETURN status 200
 *      result object with key complete :  value boolean (True if complete, False if not)
 * RETURN status 204
 *      result object with id = -1 & complete = false
 * URL(GET) : <HOST>/data/complete?id=<DATA_ID>
 */
router.get("/data_complete", (req, res) => {
  const { id } = req.query;
  const SET_CHECK_DATA = mysql.format(
    "Set @checkData = (select JSON_OBJECT( " +
      "'histogram', histogram," +
      "'integral', integral," +
      "'integral_dist', integral_dist," +
      "'raw_dist',raw_dist," +
      "'turnloss',turnloss" +
      ")  from Data where id=?);",
    [id]
  );
  const GET_JSON_LENGTH_EVERY_DATA = mysql.format(
    "select JSON_LENGTH(@checkData, '$.histogram') as Histogram," +
      "JSON_LENGTH(@checkData, '$.integral') as Integral," +
      "JSON_LENGTH(@checkData, '$.integral_dist') as Integral_Dist," +
      "JSON_LENGTH(@checkData, '$.raw_dist') as Raw_Dist," +
      "JSON_LENGTH(@checkData, '$.turnloss') as Turnloss;"
  );

  connection.query(SET_CHECK_DATA, err => {
    if (err) {
      return res.status(500).send(err);
    } else
      connection.query(GET_JSON_LENGTH_EVERY_DATA, (err, result) => {
        if (err) {
          return res.status(500).send(err);
        }
        if (result.length > 0) {
          return res.status(200).json({
            complete:
              Object.keys(result[0]).filter(
                e => result[0][e] === 0 || result[0][e] === null
              ).length === 0
          });
        } else
          return res.status(204).json({
            id: -1,
            complete: false
          });
      });
  });
});

/**
 * CREATE new empty data with no query
 * RETURN status 201
 *      id : id of newly created data
 * URL(POST) : <HOST>/new_data
 */
router.post("/new_data", (req, res) => {
  const CREATE_EMPTY_DATA = mysql.format(
    "insert into Data(histogram, integral, integral_dist, raw_dist, turnloss) " +
      "VALUES ( '{}', '{}', '{}', '{}', '{}')"
  );
  connection.query(CREATE_EMPTY_DATA, (err, result) => {
    if (err) return res.send(err);
    else {
      return res.status(201).json({
        id: result.insertId
      });
    }
  });
});

module.exports = router;
