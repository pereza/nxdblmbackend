const express = require("express");
const connection = require("../mysql/mysqlconf");
const mysql = require("mysql");
const router = express.Router();

/**
 * GET turloss_hg datapoints, the number of turns and the number of rows of a fill by id with query
 *      fill_id - fill_id of the fill we search
 *      fill_mode_id - id of the fill_mode we wan't to look at
 *      devive_id - id of the device we can't data
 * RETURN status 200
 *      rows : number of rows in the turnloss
 *      nturns : number of turns
 *      datapoints : array of data for turnloss
 * RETURN status 404
 *      rows : 0
 *      nturns : 0
 *      datapoints : [] empty array
 * URL(GET) : <HOST>/turnloss/hg?fill_id=<FILL_ID>&fill_mode_id=<FILL_MODE_ID>&device_id=<DEVICE_ID>
 */
router.get("/turnloss/hg", (req, res) => {
  const { fill_id, fill_mode_id, device_id } = req.query;
  const GET_TURNLOSS_HG = mysql.format(
    "select turnloss from Data where id=( " +
      "select data_id from Measure " +
      "inner join FillMode FM on Measure.fill_mode_id = FM.id " +
      "where device_id=? and fill_id=? and fill_mode_id=? " +
      ")",
    [device_id, fill_id, fill_mode_id]
  );
  connection.query(GET_TURNLOSS_HG, (err, results) => {
    if (results.length > 0) {
      results = JSON.parse(results[0].turnloss);
    } else {
      return res.status(204).json({
        rows: 0,
        nturns: 0,
        dataPoints: []
      });
    }
    if (err) {
      return res.status(500).send(err);
    } else {
      return res.status(200).json({
        rows: results.rows,
        nturns: results.nturns,
        dataPoints: results.turnloss_hg
      });
    }
  });
});

/**
 * GET turloss_lg datapoints, the number of turns and the number of rows of a fill by id with query
 *      fill_id - fill_id of the fill we search
 *      fill_mode_id - id of the fill_mode we wan't to look at
 *      devive_id - id of the device we can't data
 * RETURN status 200
 *      rows : number of rows in the turnloss
 *      nturns : number of turns
 *      datapoints : array of data for turnloss
 * RETURN status 404
 *      rows : 0
 *      nturns : 0
 *      datapoints : [] empty array
 * URL(GET) : <HOST>/turnloss/lg?fill_id=<FILL_ID>&fill_mode_id=<FILL_MODE_ID>&device_id=<DEVICE_ID>
 */
router.get("/turnloss/lg", (req, res) => {
  const { fill_id, fill_mode_id, device_id } = req.query;
  const GET_TURNLOSS_LG = mysql.format(
    "select turnloss from Data where id=( " +
      "select data_id from Measure " +
      "inner join FillMode FM on Measure.fill_mode_id = FM.id " +
      "where device_id=? and fill_id=? and fill_mode_id=? " +
      ")",
    [device_id, fill_id, fill_mode_id]
  );
  connection.query(GET_TURNLOSS_LG, (err, results) => {
    if (results.length > 0) {
      results = JSON.parse(results[0].turnloss);
    } else {
      return res.status(204).json({
        rows: 0,
        nturns: 0,
        dataPoints: []
      });
    }
    if (err) {
      return res.status(500).send(err);
    } else {
      return res.status(200).json({
        rows: results.rows,
        nturns: results.nturns,
        dataPoints: results.turnloss_lg
      });
    }
  });
});

/**
 * GET turloss_hg intime_datapoints, timestamps, the number of turns and the number of rows of a fill by id with query
 *      fill_id - fill_id of the fill we search
 *      fill_mode_id - id of the fill_mode we wan't to look at
 *      devive_id - id of the device we can't data
 * RETURN status 200
 *      rows : number of rows in the turnloss
 *      nturns : number of turns
 *      timestamps : array of timstamps
 *      datapoints : array of data for turnloss
 * RETURN status 404
 *      rows : 0
 *      nturns : 0
 *      timestamps : [] empty array
 *      datapoints : [] empty array
 * URL(GET) : <HOST>/turnloss/hg_intime?fill_id=<FILL_ID>&fill_mode_id=<FILL_MODE_ID>&device_id=<DEVICE_ID>
 */
router.get("/turnloss/hg_intime", (req, res) => {
  const { fill_id, fill_mode_id, device_id } = req.query;
  const GET_TURNLOSS_INTIME_HG = mysql.format(
    "select turnloss from Data where id=( " +
      "select data_id from Measure " +
      "inner join FillMode FM on Measure.fill_mode_id = FM.id " +
      "where device_id=? and fill_id=? and fill_mode_id=? " +
      ")",
    [device_id, fill_id, fill_mode_id]
  );
  connection.query(GET_TURNLOSS_INTIME_HG, (err, results) => {
    if (results.length > 0) {
      results = JSON.parse(results[0].turnloss);
    } else {
      return res.status(204).json({
        rows: 0,
        nturns: 0,
        dataPoints: [],
        timestamps: []
      });
    }
    if (err) {
      return res.status(500).send(err);
    } else {
      return res.status(200).json({
        rows: results.rows,
        nturns: results.nturns,
        dataPoints: results.turnloss_hg_intime,
        timestamps: results.timestamps
      });
    }
  });
});

/**
 * GET turloss_lg intime_datapoints, timestamps, the number of turns and the number of rows of a fill by id with query
 *      fill_id - fill_id of the fill we search
 *      fill_mode_id - id of the fill_mode we wan't to look at
 *      devive_id - id of the device we can't data
 * RETURN status 200
 *      rows : number of rows in the turnloss
 *      nturns : number of turns
 *      timestamps : array of timstamps
 *      datapoints : array of data for turnloss
 * RETURN status 204
 *      rows : 0
 *      nturns : 0
 *      timestamps : [] empty array
 *      datapoints : [] empty array
 * URL(GET) : <HOST>/turnloss/lg_intime?fill_id=<FILL_ID>&fill_mode_id=<FILL_MODE_ID>&device_id=<DEVICE_ID>
 */
router.get("/turnloss/lg_intime", (req, res) => {
  const { fill_id, fill_mode_id, device_id } = req.query;
  const GET_TURNLOSS_INTIME_LG = mysql.format(
    "select turnloss from Data where id=( " +
      "select data_id from Measure " +
      "inner join FillMode FM on Measure.fill_mode_id = FM.id " +
      "where device_id=? and fill_id=? and fill_mode_id=? " +
      ")",
    [device_id, fill_id, fill_mode_id]
  );
  connection.query(GET_TURNLOSS_INTIME_LG, (err, results) => {
    if (results.length > 0) {
      results = JSON.parse(results[0].turnloss);
    } else {
      return res.status(204).json({
        rows: 0,
        nturns: 0,
        dataPoints: [],
        timestamps: []
      });
    }
    if (err) {
      return res.status(500).send(err);
    } else {
      return res.status(200).json({
        rows: results.rows,
        nturns: results.nturns,
        dataPoints: results.turnloss_lg_intime,
        timestamps: results.timestamps
      });
    }
  });
});

/**
 * UPDATE turnloss with query
 *      id - id of the data row we wan't to update
 *              and body
 *      data - turnloss object
 * RETURN status 200 - Turnloss data updated
 * RETURN other status - Error during update
 * URL(PUT) : <HOST>/turnloss?id=<DATA_ID>
 */
router.put("/turnloss", (req, res) => {
  const { id } = req.query;
  const data = req.body;
  const UPDATE_TURNLOSS_FOR_DATA_ID = mysql.format(
    "UPDATE Data SET turnloss = ? where id = ?",
    [JSON.stringify(data), id]
  );
  connection.query(UPDATE_TURNLOSS_FOR_DATA_ID, (err, result) => {
    if (err) return res.status(500).send(err);
    else console.log(result);
    return res.sendStatus(200);
  });
});
module.exports = router;
