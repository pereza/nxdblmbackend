const express = require("express");
const connection = require("../mysql/mysqlconf");
const mysql = require("mysql");
const router = express.Router();

/**
 * GET integral_hg datapoints and number of rows of a fill by id with query
 *      fill_id - fill_id of the fill we search
 *      fill_mode_id - id of the fill_mode we wan't to look at
 *      devive_id - id of the device we can't data
 * RETURN status 200
 *      rows : number of rows in the integral
 *      datapoints : array of data for integral
 * RETURN status 204
 *      rows : 0
 *      datapoints : [] empty array
 * URL(GET) : <HOST>/integral/hg?fill_id=<FILL_ID>&fill_mode_id=<FILL_MODE_ID>&device_id=<DEVICE_ID>
 */
router.get("/integral/hg", (req, res) => {
  const { fill_id, fill_mode_id, device_id } = req.query;
  const GET_INTEGRAL_HG = mysql.format(
    "select integral from Data where id=( " +
      "select data_id from Measure " +
      "inner join FillMode FM on Measure.fill_mode_id = FM.id " +
      "where device_id=? and fill_id=? and fill_mode_id=? " +
      ")",
    [device_id, fill_id, fill_mode_id]
  );
  connection.query(GET_INTEGRAL_HG, (err, results) => {
    if (results.length > 0) {
      results = JSON.parse(results[0].integral);
    } else {
      return res.status(204).json({
        rows: 0,
        dataPoints: []
      });
    }
    if (err) {
      return res.status(500).send(err);
    } else {
      return res.status(200).json({
        rows: results.rows,
        dataPoints: results.integral_hg
      });
    }
  });
});

/**
 * GET integral_lg datapoints and number of rows of a fill by id with query
 *      fill_id - fill_id of the fill we search
 *      fill_mode_id - id of the fill_mode we wan't to look at
 *      devive_id - id of the device we can't data
 * RETURN status 200
 *      rows : number of rows in the integral
 *      datapoints : array of data for integral
 * RETURN status 404
 *      rows : 0
 *      datapoints : [] empty array
 * URL(GET) : <HOST>/integral/lg?fill_id=<FILL_ID>&fill_mode_id=<FILL_MODE_ID>&device_id=<DEVICE_ID>
 */
router.get("/integral/lg", (req, res) => {
  const { fill_id, fill_mode_id, device_id } = req.query;
  const GET_INTEGRAL_LG = mysql.format(
    "select integral from Data where id=( " +
      "select data_id from Measure " +
      "inner join FillMode FM on Measure.fill_mode_id = FM.id " +
      "where device_id=? and fill_id=? and fill_mode_id=? " +
      ")",
    [device_id, fill_id, fill_mode_id]
  );
  connection.query(GET_INTEGRAL_LG, (err, results) => {
    if (results.length > 0) {
      results = JSON.parse(results[0].integral);
    } else {
      return res.status(204).json({
        rows: 0,
        dataPoints: []
      });
    }
    if (err) {
      return res.status(500).send(err);
    } else {
      return res.status(200).json({
        rows: results.rows,
        dataPoints: results.integral_lg
      });
    }
  });
});

/**
 * GET integral_hg intime_datapoints, timestamps and number of rows of a fill by id with query
 *      fill_id - fill_id of the fill we search
 *      fill_mode_id - id of the fill_mode we wan't to look at
 *      devive_id - id of the device we can't data
 * RETURN status 200
 *      rows : number of rows in the integral
 *      datapoints : array of data for integral
 *      timestamps : array of timestamps for integral
 * RETURN status 204
 *      rows : 0
 *      datapoints : [] empty array
 *      timestamps : [] empty array
 * URL(GET) : <HOST>/integral/hg_intime?fill_id=<FILL_ID>&fill_mode_id=<FILL_MODE_ID>&device_id=<DEVICE_ID>
 */
router.get("/integral/hg_intime", (req, res) => {
  const { fill_id, fill_mode_id, device_id } = req.query;
  const GET_INTEGRAL_HG_INTIME = mysql.format(
    "select integral from Data where id=( " +
      "select data_id from Measure " +
      "inner join FillMode FM on Measure.fill_mode_id = FM.id " +
      "where device_id=? and fill_id=? and fill_mode_id=? " +
      ")",
    [device_id, fill_id, fill_mode_id]
  );
  connection.query(GET_INTEGRAL_HG_INTIME, (err, results) => {
    if (results.length > 0) {
      results = JSON.parse(results[0].integral);
    } else {
      return res.status(204).json({
        rows: 0,
        dataPoints: [],
        timestamps: []
      });
    }
    if (err) {
      return res.status(500).send(err);
    } else {
      return res.status(200).json({
        rows: results.rows,
        dataPoints: results.integral_hg_intime,
        timestamps: results.timestamps
      });
    }
  });
});

/**
 * GET integral_lg intime_datapoints, timestamps and number of rows of a fill by id with query
 *      fill_id - fill_id of the fill we search
 *      fill_mode_id - id of the fill_mode we wan't to look at
 *      devive_id - id of the device we can't data
 * RETURN status 200
 *      rows : number of rows in the integral
 *      datapoints : array of data for integral
 *      timestamps : array of timestamps for integral
 * RETURN status 204
 *      rows : 0
 *      datapoints : [] empty array
 *      timestamps : [] empty array
 * URL(GET) : <HOST>/integral/lg_intime?fill_id=<FILL_ID>&fill_mode_id=<FILL_MODE_ID>&device_id=<DEVICE_ID>
 */
router.get("/integral/lg_intime", (req, res) => {
  const { fill_id, fill_mode_id, device_id } = req.query;
  const GET_INTEGRAL_LG_INTIME = mysql.format(
    "select integral from Data where id=( " +
      "select data_id from Measure " +
      "inner join FillMode FM on Measure.fill_mode_id = FM.id " +
      "where device_id=? and fill_id=? and fill_mode_id=? " +
      ")",
    [device_id, fill_id, fill_mode_id]
  );
  connection.query(GET_INTEGRAL_LG_INTIME, (err, results) => {
    if (results.length > 0) {
      results = JSON.parse(results[0].integral);
    } else {
      return res.status(204).json({
        rows: 0,
        dataPoints: [],
        timestamps: []
      });
    }
    if (err) {
      return res.status(500).send(err);
    } else {
      return res.status(200).json({
        rows: results.rows,
        dataPoints: results.integral_lg_intime,
        timestamps: results.timestamps
      });
    }
  });
});

/**
 * UPDATE integral with query
 *      id - id of the data row we wan't to update
 *              and body
 *      data - integral object
 * RETURN status 200 - integral data updated
 * RETURN other status - Error during update
 * URL(PUT) : <HOST>/integral?id=<DATA_ID>
 */
router.put("/integral", (req, res) => {
  const { id } = req.query;
  const data = req.body;
  const UPDATE_INTEGRAL_FOR_DATA_ID = mysql.format(
    "UPDATE Data SET integral = ? where id = ?",
    [JSON.stringify(data), id]
  );
  connection.query(UPDATE_INTEGRAL_FOR_DATA_ID, (err, result) => {
    if (err) return res.status(500).send(err);
    else return res.sendStatus(200);
  });
});

module.exports = router;
