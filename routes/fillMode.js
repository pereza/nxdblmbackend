const express = require("express");
const connection = require("../mysql/mysqlconf");
const mysql = require("mysql");
const router = express.Router();

/**
 * GET fill-mode with query
 *      mode-name - name of the mode of the fill-mode
 *      fill-name - name of the fill of the fill-mode
 *      start - string date /!\ no spaces allowed in URL --> use '_' instead
 *      end - string date /!\ no spaces allowed in URL --> use '_' instead
 * RETURN status 200
 *      fill-mode object (id, fill-name, mode-name, start, end)
 * RETURN status 204
 *      id: -1
 * URL(GET) : <HOST>/fill_mode?mode_name=<MODE_NAME>&fill_name=<FILL_NAME>&start=<START>&end=<END>
 */
router.get("/fill_mode", (req, res) => {
  const { mode_name, fill_name, start, end } = req.query;
  const GET_FILL_MODE_ID = mysql.format(
    "select * from FillMode where mode_id=(select id from Mode where name=?)" +
      "and fill_id=(select id from Fill where name=?) " +
      "and start=? and end=?",
    [mode_name, fill_name, start, end]
  );
  connection.query(GET_FILL_MODE_ID, (err, result) => {
    if (err) return res.status(500).send(err);
    if (result.length > 0) {
      return res.status(200).json({
        ...result[0]
      });
    } else
      return res.status(204).json({
        id: -1
      });
  });
});

/**
 * CREATE new fill-mode with query
 *      fill_name - name of the fill of the new fill_mode
 *      mode_name - name of the mode of the new fill_mode
 *      start - string date /!\ no spaces allowed in URL --> use '_' instead
 *      end - string date /!\ no spaces allowed in URL --> use '_' instead
 * RETURN status 201
 *      id : id of newly created fill
 * URL(POST) : <HOST>/new_fill_mode?fill_name=<FILL_NAME>&mode_name=<MODE_NAME>&start=<START>&end=<END>
 */
router.post("/new_fill_mode", (req, res) => {
  const { fill_name, mode_name, start, end } = req.query;
  const CREATE_NEW_FILL_MODE = mysql.format(
    "insert into FillMode(fill_id, mode_id, start, end)" +
      "values ((select id from Fill where name=?),(select id from Mode where name=?), ?, ?)",
    [fill_name, mode_name, start, end]
  );
  if (fill_name && mode_name && start && end)
    connection.query(CREATE_NEW_FILL_MODE, (err, result) => {
      if (err) return res.status(500).send(err);
      else {
        return res.status(201).json({
          id: result.insertId
        });
      }
    });
  else return res.sendStatus(400);
});

module.exports = router;
