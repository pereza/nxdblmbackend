const express = require("express");
const connection = require("../mysql/mysqlconf");
const mysql = require("mysql");
const router = express.Router();

/**
 * GET measure with query
 *      device_id - device_id of he measure we search
 *      mode_name -
 *      fill_name -
 *      start - string date /!\ no spaces allowed in URL --> use '_' instead
 * RETURN status 200
 *      mode object (id, name)
 * RETURN status 204
 *      id: -1
 * URL(GET) : <HOST>/measure?device_id=<DEVICE_ID>&mode_name=<MODE_NAME>&fill_name=<FILL_NAME>&start=<START>
 */
router.get("/measure", (req, res) => {
  const { device_id, mode_name, fill_name, start } = req.query;
  const GET_MEASURE = mysql.format(
    "select * from Measure where device_id=?" +
      "and fill_mode_id=( select id from FillMode " +
      "where mode_id=(select id from Mode where name=?)" +
      "and fill_id=(select id from Fill where name=?)" +
      "and start=?)",
    [device_id, mode_name, fill_name, start]
  );
  connection.query(GET_MEASURE, (err, result) => {
    if (err) {
      return res.status(500).send(err);
    }
    if (result.length > 0) {
      console.log(result);
      return res.status(200).json({
        ...result[0]
      });
    } else {
      return res.status(204).json({
        id: -1
      });
    }
  });
});

/**
 * CREATE new measure with query
 *      fill_mode_id
 *      data_id
 *      device_id
 * RETURN status 201
 *      id : id of newly created measure
 * URL(POST) : <HOST>/new_measure?fill_mode_id=<FILL_MODE_ID>&data_id=<DATA_ID>&device_id=<DATA_ID>
 */
router.post("/new_measure", (req, res) => {
  const { fill_mode_id, data_id, device_id } = req.query;
  const CREATE_NEW_MEASURE = mysql.format(
    "INSERT INTO Measure(fill_mode_id, data_id, device_id) " +
      "VALUES ( ?, ?, ? )",
    [fill_mode_id, data_id, device_id]
  );
  if (fill_mode_id && data_id && device_id)
    connection.query(CREATE_NEW_MEASURE, (err, result) => {
      if (err) return res.status(500).send(err);
      else {
        console.log(result);
        return res.status(201).json({
          id: result.insertId
        });
      }
    });
  else return res.sendStatus(400);
});

module.exports = router;
