const express = require("express");
const cors = require("cors");
const bodyParser = require("body-parser");

const app = express();

app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json({ limit: "2000mb" }));
app.use(bodyParser.urlencoded({ extended: true, limit: "2000mb" }));

const fillsRouter = require("./routes/fills");
const modesRouter = require("./routes/modes");
const deviceRouter = require("./routes/devices");
const measureRouter = require("./routes/measures");
const dataRouter = require("./routes/data");
const fillModeRouter = require("./routes/fillMode");

const histogramRouter = require("./routes/histogram");
const integralRouter = require("./routes/integral");
const integralDistRouter = require("./routes/integralDist");
const rawDistRouter = require("./routes/rawDist");
const turnlossRouter = require("./routes/turnloss");

app.use(fillsRouter);
app.use(modesRouter);
app.use(deviceRouter);
app.use(measureRouter);
app.use(dataRouter);
app.use(fillModeRouter);

app.use(histogramRouter);
app.use(integralRouter);
app.use(integralDistRouter);
app.use(rawDistRouter);
app.use(turnlossRouter);

app.listen(8080, () => {
  console.log("Data server listening on port 8080");
});
