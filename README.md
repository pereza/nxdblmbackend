# API NXCALS dblm Back-End

JavaScript API used to contact the MySQL DataBase of the 'webpage for dblm monitoring' project.

## Installation

This API is easely deployable on CERN [web service's](https://webservices.web.cern.ch/webservices/) server.
Here we used PaaS Web Application type. This type of servers is provided with an OpenShift project. 
 
As soon as your web server is created you will receive an e-mail with a link to your server's dash board. Go to the OpenShift project, then link the API's git repo to the project as Node.js project. 

If everything gone correctly, a pod will be created, and your API is online. 

## Usage

### Data:

 * GET data by *id* with query
 *     <HOST>/data?id=<DATA_ID>
 
 * CREATE new empty data with no query
 *      URL(POST) : <HOST>/new_data

### Devices:

 * GET device by *name* with query
 *     <HOST>/device?name=<DEVICE_NAME>

 * GET all devices with no query
 *     <HOST>/devices
 
### Fill Mode:
 
  * GET fill-mode with query 
  > * mode-name - name of the mode of the fill-mode
  > * fill-name - name of the fill of the fill-mode
  > * start - string date /!\ no spaces allowed in URL --> use '_' instead
  > * end - string date /!\ no spaces allowed in URL --> use '_' instead
  *     <HOST>/fill_mode?mode_name=<MODE_NAME>&fill_name=<FILL_NAME>&start=<START>&end=<END>

 * CREATE new fill-mode with query
 > * fill_name - name of the fill of the new fill_mode
 > * mode_name - name of the mode of the new fill_mode
 > * start - string date /!\ no spaces allowed in URL --> use '_' instead
 > * end - string date /!\ no spaces allowed in URL --> use '_' instead
 *     <HOST>/new_fill_mode?fill_name=<FILL_NAME>&mode_name=<MODE_NAME>&start=<START>&end=<END>
 
### Fills:
 
 * GET all fills with no query
 *     <HOST>/fills
  
 * GET fill by *id* with query
 *     <HOST>/fill/metadata/id?id=<FILL_ID>

 * GET fill by *name* with query
 *     <HOST>/fill/metadata/id?id=<FILL_ID>
 
 * GET **modes** of a fill by *id* with query
 *     <HOST>/fill/modes/id?id=<FILL_ID>
  
 * GET **modes** of a fill by *name* with query
 *     <HOST>/fill/modes/name?name=<FILL_NAME>
   
 * GET **devices** of a fill by *id* with query
 *     <HOST>/fill/devices/id?id=<FILL_ID>
 
 * GET **devices** of a fill by *id* with query
 *     <HOST>/fill/devices/name?name=<FILL_NAME>
 
 * GET last fill with no query
 *     <HOST>/fill/last
 
 * CREATE new fill with query
 > *  name - name of the fill we wan't to create
 > *  start - string date /!\ no spaces allowed in URL --> use '_' instead
 > *  end - string date /!\ no spaces allowed in URL --> use '_' instead
 *     <HOST>/new_fill?name=<FILL_NAME>&start=<START>&end=<END>
 
### Histogram:
 
 * GET histogram_hg datapoints and number of rows of a fill by id with query
 > * fill_id - fill_id of the fill we search
 > * fill_mode_id - id of the fill_mode we wan't to look at
 > * devive_id - id of the device we can't data
 *     <HOST>/histogram/hg?fill_id=<FILL_ID>&fill_mode_id=<FILL_MODE_ID>&device_id=<DEVICE_ID>
 
 * GET histogram_lg datapoints and number of rows of a fill by id with query
 > * fill_id - fill_id of the fill we search
 > * fill_mode_id - id of the fill_mode we wan't to look at
 > * devive_id - id of the device we can't data
 *     <HOST>/histogram/lg?fill_id=<FILL_ID>&fill_mode_id=<FILL_MODE_ID>&device_id=<DEVICE_ID>
 
 * GET histogram_hg intime_datapoints, timestamps and number of rows of a fill by id with query
 > * fill_id - fill_id of the fill we search
 > * fill_mode_id - id of the fill_mode we wan't to look at
 > * devive_id - id of the device we can't data
 *     <HOST>/histogram/hg_intime?fill_id=<FILL_ID>&fill_mode_id=<FILL_MODE_ID>&device_id=<DEVICE_ID>
 
 * GET histogram_lg intime_datapoints, timestamps and number of rows of a fill by id with query
 > * fill_id - fill_id of the fill we search
 > * fill_mode_id - id of the fill_mode we wan't to look at
 > * devive_id - id of the device we can't data
 *     <HOST>/histogram/hl_intime?fill_id=<FILL_ID>&fill_mode_id=<FILL_MODE_ID>&device_id=<DEVICE_ID>
 
  * UPDATE histogram
 > with query:
 > id - id of the data row we wan't to update
 
 > and body:
 > data - histogram object
  *     <HOST>/histogram?id=<DATA_ID>
  
### Integral:
 * GET integral_hg datapoints and number of rows of a fill by id with query
 > * fill_id - fill_id of the fill we search
 > * fill_mode_id - id of the fill_mode we wan't to look at
 > * devive_id - id of the device we can't data
 *     <HOST>/integral/hg?fill_id=<FILL_ID>&fill_mode_id=<FILL_MODE_ID>&device_id=<DEVICE_ID>
 
 * GET integral_lg datapoints and number of rows of a fill by id with query
 > * fill_id - fill_id of the fill we search
 > * fill_mode_id - id of the fill_mode we wan't to look at
 > * devive_id - id of the device we can't data
 *     <HOST>/integral/lg?fill_id=<FILL_ID>&fill_mode_id=<FILL_MODE_ID>&device_id=<DEVICE_ID>
 
 * GET integral_hg intime_datapoints, timestamps and number of rows of a fill by id with query
 > * fill_id - fill_id of the fill we search
 > * fill_mode_id - id of the fill_mode we wan't to look at
 > * devive_id - id of the device we can't data
 *     <HOST>/integral/hg_intime?fill_id=<FILL_ID>&fill_mode_id=<FILL_MODE_ID>&device_id=<DEVICE_ID>

 * GET integral_lg intime_datapoints, timestamps and number of rows of a fill by id with query
 > * fill_id - fill_id of the fill we search
 > * fill_mode_id - id of the fill_mode we wan't to look at
 > * devive_id - id of the device we can't data
 *     <HOST>/integral/lg_intime?fill_id=<FILL_ID>&fill_mode_id=<FILL_MODE_ID>&device_id=<DEVICE_ID>
 
  * UPDATE integral with query
  > id - id of the data row we wan't to update
  
> and body
  > data - integral object
  * URL(PUT) : <HOST>/integral?id=<DATA_ID>
  
### Integral Distance:
 * GET integral_dist_hg datapoints and number of rows of a fill by id with query
 > * fill_id - fill_id of the fill we search
 > * fill_mode_id - id of the fill_mode we wan't to look at
 > * devive_id - id of the device we can't data
 *  <HOST>/integral_dist/hg?fill_id=<FILL_ID>&fill_mode_id=<FILL_MODE_ID>&device_id=<DEVICE_ID>
 
 * GET integral_dist_lg datapoints and number of rows of a fill by id with query
 > * fill_id - fill_id of the fill we search
 > * fill_mode_id - id of the fill_mode we wan't to look at
 > * devive_id - id of the device we can't data
 *     <HOST>/integral_dist/lg?fill_id=<FILL_ID>&fill_mode_id=<FILL_MODE_ID>&device_id=<DEVICE_ID>
 
 * UPDATE integral_dist with query
 > id - id of the data row we wan't to update
 
 >and body: 
 >data - integral_dist object
 * URL(PUT) : <HOST>/integral_dist?id=<DATA_ID>

### Measures:

 * GET measure with query
 > * device_id - device_id of the measure we search
 > * mode_name - mode_name of the measure we search
 > * fill_name - fill_name of the measure we search
 > * start - string date /!\ no spaces allowed in URL --> use '_' instead
 *     <HOST>/measure?device_id=<DEVICE_ID>&mode_name=<MODE_NAME>&fill_name=<FILL_NAME>&start=<START>
 
 * CREATE new measure with query
 > * fill_mode_id 
 > * data_id
 > * device_id
 *     <HOST>/new_measure?fill_mode_id=<FILL_MODE_ID>&data_id=<DATA_ID>&device_id=<DATA_ID>

### Modes:
 * GET mode with query
 > *  name - name of the mode we are searching for
 *     <HOST>/mode?name=<MODE_NAME>
 
#Raw Distance:
 
  * GET raw_dist_hg datapoints and number of rows of a fill by id with query
  > * fill_id - fill_id of the fill we search
  > * fill_mode_id - id of the fill_mode we wan't to look at
  > * devive_id - id of the device we can't data
  *     <HOST>/raw_dist/hg?fill_id=<FILL_ID>&fill_mode_id=<FILL_MODE_ID>&device_id=<DEVICE_ID>

 * GET raw_dist_lg datapoints and number of rows of a fill by id with query
 > * fill_id - fill_id of the fill we search
 > * fill_mode_id - id of the fill_mode we wan't to look at
 > * devive_id - id of the device we can't data
 *     <HOST>/raw_dist/lg?fill_id=<FILL_ID>&fill_mode_id=<FILL_MODE_ID>&device_id=<DEVICE_ID>
 
  * UPDATE raw_dist with query
  > id - id of the data row we wan't to update
  
  > and body:
  > data - raw_dist object
  * URL(PUT) : <HOST>/raw_dist?id=<DATA_ID>
  
### Turnloss:
  
 * GET turloss_hg datapoints, the number of turns and the number of rows of a fill by id with query
 > * fill_id - fill_id of the fill we search
 > * fill_mode_id - id of the fill_mode we wan't to look at
 > * devive_id - id of the device we can't data
 *     <HOST>/turnloss/hg?fill_id=<FILL_ID>&fill_mode_id=<FILL_MODE_ID>&device_id=<DEVICE_ID>

 * GET turloss_lg datapoints, the number of turns and the number of rows of a fill by id with query
 > * fill_id - fill_id of the fill we search
 > * fill_mode_id - id of the fill_mode we wan't to look at
 > * devive_id - id of the device we can't data
 *     <HOST>/turnloss/lg?fill_id=<FILL_ID>&fill_mode_id=<FILL_MODE_ID>&device_id=<DEVICE_ID>
 
  * GET turloss_hg intime_datapoints, timestamps, the number of turns and the number of rows of a fill by id with query
  > * fill_id - fill_id of the fill we search
  > * fill_mode_id - id of the fill_mode we wan't to look at
  > * devive_id - id of the device we can't data
  *     <HOST>/turnloss/hg_intime?fill_id=<FILL_ID>&fill_mode_id=<FILL_MODE_ID>&device_id=<DEVICE_ID>
  
 * GET turloss_lg intime_datapoints, timestamps, the number of turns and the number of rows of a fill by id with query
 > * fill_id - fill_id of the fill we search
 > * fill_mode_id - id of the fill_mode we wan't to look at
 > * devive_id - id of the device we can't data
 *     <HOST>/turnloss/lg_intime?fill_id=<FILL_ID>&fill_mode_id=<FILL_MODE_ID>&device_id=<DEVICE_ID>

 * UPDATE turnloss with query
 > id - id of the data row we wan't to update
 
 > and body:
 > data - turnloss object
 *     <HOST>/turnloss?id=<DATA_ID>
